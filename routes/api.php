<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProjectController;
use App\Http\Controllers\API\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::get('list', [AuthController::class, 'index']);
//Route::post('create', [UserController::class, 'store'])->name('create');
Route::get('show/{id}', [AuthController::class, 'show']);
Route::post('update/{id}', [AuthController::class, 'update']);
Route::delete('destroy/{id}', [AuthController::class, 'destroy']);

Route::apiResource('projects', ProjectController::class)->middleware('auth:api');