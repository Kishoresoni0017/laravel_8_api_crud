<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RelationshipController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  //  return view('users');
//});


Route::get('/', [RelationshipController::class, 'users'])->name('users');
Route::get('posts', [RelationshipController::class, 'posts'])->name('posts');
Route::get('projects', [RelationshipController::class, 'projects'])->name('projects');

Route::get('emails', [RelationshipController::class, 'emails'])->name('emails');
