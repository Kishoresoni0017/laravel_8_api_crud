<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Products;
use App\Models\Posts;
use App\Models\Tags;
use App\Models\Project;
use App\Models\Task;
use App\Jobs\SendMail;

class RelationshipController extends Controller
{

//For get single data of other table(products) in user table
   // public function users()
   // {
   // 	$users=User::get();
         
    //	return view('users',compact('users'));
    //}
 
 //For get multiple data of other table(products) in user table
   public function users()
    {
    	$users=User::with('products')->get();
         
    	return view('users',compact('users'));
    }

  //For get data form user table and showing with products table in product list.
    public function products()
    {
    	$products=Products::get();

    	return view('users',compact('products'));
    }

    //For get data form posts table and showing with users and tags table in posts list.
    public function posts()
    {
    	$posts=Posts::with(['users','tags'])->get();

    	return view('posts',compact('posts'));
    }

     //For get data form projects table and showing with users and task table in projects list.
    public function projects()
    {
    	$projects=Project::with(['users','tasks'])->get();
       // dd($projects);
    	
    	return view('projects',compact('projects'));
    }

    public function emails()
    {
    	


    	SendMail::dispatch(new SendMail);


    	dd('done');	
    }

}
