<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
        'post_name',
        
    ];

     public function users()
    {
      return $this->belongsTo(User::class,'user_id','id');
    }

    public function tags()
    {
      return $this->belongsToMany(Tags::class,'post_tag','post_id','tag_id')
                  ->withTimestamps()
                  ->withPivot('status');
    }
}
