<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'location',
        'introduction',
        'cost',
    ];


    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'cost' => 'int',
    ];

     public function users()
    {
      return $this->hasMany(User::class, 'project_id','id');
    }

    public function tasks()
    {
      return $this->hasManyThrough(Task::class,User::class,'project_id','user_id','id');
    }

     public function task()
    {
      return $this->hasOneThrough(Task::class,User::class,'project_id','user_id','id');
    }
}
