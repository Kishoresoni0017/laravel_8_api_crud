<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SendMail as SendEmailTestMail;
use Mail;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected  $details;
    

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //dd('ok');
        
        $this->details = 'kishor.bitcot@gmail.com';
        $email = new SendEmailTestMail();
        Mail::to($this->details)->send($email);
    }
}
